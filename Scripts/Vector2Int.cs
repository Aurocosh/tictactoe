﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

public readonly struct Vector2Int : IEquatable<Vector2Int>, IFormattable
{
    public int X { get; }
    public int Y { get; }

    public Vector2Int(int value)
    {
        X = value;
        Y = value;
    }

    public Vector2Int(int x, int y)
    {
        X = x;
        Y = y;
    }

    public static Vector2Int Zero { get { return new Vector2Int(0, 0); } }

    #region Public instance methods

    #region Equality methods

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public override bool Equals(object obj)
    {
        if (!(obj is Vector2Int))
            return false;
        return Equals((Vector2Int)obj);
    }

    public bool Equals(Vector2Int other)
    {
        return X == other.X && Y == other.Y;
    }

    public override int GetHashCode()
    {
        unchecked // Overflow is fine, just wrap
        {
            int hash = 17;
            hash = (hash * 23) + X.GetHashCode();
            hash = (hash * 23) + Y.GetHashCode();
            return hash;
        }
    }

    #endregion Equality methods

    #region To string methods

    public override string ToString()
    {
        return ToString("D", CultureInfo.CurrentCulture);
    }

    public string ToString(string format)
    {
        return ToString(format, CultureInfo.CurrentCulture);
    }

    public string ToString(string format, IFormatProvider formatProvider)
    {
        var sb = new StringBuilder();
        string separator = NumberFormatInfo.GetInstance(formatProvider).NumberGroupSeparator;
        sb.Append('<');
        sb.Append(X.ToString(format, formatProvider));
        sb.Append(separator);
        sb.Append(' ');
        sb.Append(Y.ToString(format, formatProvider));
        sb.Append('>');
        return sb.ToString();
    }

    #endregion To string methods

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float Length()
    {
        int squareLength = (X * X) + (Y * Y);
        return (float)Math.Sqrt(squareLength);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public int LengthSquared()
    {
        return (X * X) + (Y * Y);
    }

    #endregion Public instance methods

    #region Public static methods

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float Distance(Vector2Int value1, Vector2Int value2)
    {
        float dx = value1.X - value2.X;
        float dy = value1.Y - value2.Y;

        float ls = (dx * dx) + (dy * dy);

        return (float)Math.Sqrt(ls);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float DistanceSquared(Vector2Int value1, Vector2Int value2)
    {
        float dx = value1.X - value2.X;
        float dy = value1.Y - value2.Y;

        return (dx * dx) + (dy * dy);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Clamp(Vector2Int value1, Vector2Int min, Vector2Int max)
    {
        // This compare order is very important!!!
        // We must follow HLSL behavior in the case user specified min value is bigger than max value.
        int x = value1.X;
        x = (x > max.X) ? max.X : x;
        x = (x < min.X) ? min.X : x;

        int y = value1.Y;
        y = (y > max.Y) ? max.Y : y;
        y = (y < min.Y) ? min.Y : y;

        return new Vector2Int(x, y);
    }

    #endregion Public static methods

    #region Public operator methods

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int operator +(Vector2Int left, Vector2Int right) => new Vector2Int(left.X + right.X, left.Y + right.Y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int operator -(Vector2Int left, Vector2Int right) => new Vector2Int(left.X - right.X, left.Y - right.Y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int operator *(Vector2Int left, Vector2Int right) => new Vector2Int(left.X * right.X, left.Y * right.Y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int operator /(Vector2Int left, Vector2Int right) => new Vector2Int(left.X / right.X, left.Y / right.Y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int operator *(Vector2Int left, int scalar) => new Vector2Int(left.X * scalar, left.Y * scalar);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int operator *(int scalar, Vector2Int right) => new Vector2Int(right.X * scalar, right.Y * scalar);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int operator /(Vector2Int left, int scalar) => new Vector2Int(left.X / scalar, left.Y / scalar);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int operator -(Vector2Int vector) => new Vector2Int(-vector.X, -vector.Y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Add(Vector2Int left, Vector2Int right) => left + right;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Subtract(Vector2Int left, Vector2Int right) => left - right;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Multiply(Vector2Int left, Vector2Int right) => left * right;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Multiply(Vector2Int left, int right) => left * right;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Multiply(int left, Vector2Int right) => left * right;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Divide(Vector2Int left, Vector2Int right) => left / right;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Divide(Vector2Int left, int divisor) => left / divisor;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Vector2Int Negate(Vector2Int value) => -value;

    #endregion Public operator methods
}
