using Godot;
using System;

public class Settings : Node
{
    public int GridSize { get; set; }
    public int MatchCount { get; set; }
}
