using System;
using Godot;

public class ParWrapper : Godot.Object
{
    private readonly object value;

    public ParWrapper()
    {
        this.value = null;
    }

    public ParWrapper(object value)
    {
        this.value = value;
    }

    public T GetValue<T>()
    {
        return (T)this.value;
    }
}