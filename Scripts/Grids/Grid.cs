﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Grid<T> : IReadOnlyGrid<T>
{
    public int Width => _tiles.GetLength(0);
    public int Height => _tiles.GetLength(1);

    protected readonly T[,] _tiles;

    public T this[int x, int y]
    {
        get => _tiles[x, y];
        set => _tiles[x, y] = value;
    }

    public T this[Vector2Int point]
    {
        get => _tiles[point.X, point.Y];
        set => _tiles[point.X, point.Y] = value;
    }

    public Grid()
    {
        _tiles = new T[0, 0];
    }

    public Grid(int width, int height)
    {
        _tiles = new T[width, height];
    }

    public Grid(T[,] tiles)
    {
        _tiles = tiles;
    }

    public bool IsItInBounds(int x, int y)
    {
        return x >= 0 && y >= 0 && x < Width && y < Height;
    }

    public bool IsItInBounds(Vector2Int point)
    {
        return point.X >= 0 && point.Y >= 0 && point.X < Width && point.Y < Height;
    }
}