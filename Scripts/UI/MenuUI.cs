using Godot;
using System;

public class MenuUI : CenterContainer
{
    public int MaxSize { get => _maxSize; set => _maxSize = Math.Max(value, 3); }

    private int _maxSize = 10;
    private int _minSize = 3;

    private int _gridSize = 3;
    private int _matchCount = 3;

    private Label _sizeLabel;
    private Label _matchLabel;

    public override void _Ready()
    {
        _sizeLabel = GetNode<Label>("MarginContainer/PanelContainer/ButtonsContainer/SizeContainer/SizePanel/SizeLabel");
        _matchLabel = GetNode<Label>("MarginContainer/PanelContainer/ButtonsContainer/MatchContainer/MatchPanel/MatchLabel");
    }

    public void OnMinusSizePressed()
    {
        _gridSize = Mathf.Max(_gridSize - 1, _minSize);
        _matchCount = Mathf.Min(_matchCount, _gridSize); ;
        RefreshGridCount();
        RefreshMatchCount();
    }

    public void OnPlusSizePressed()
    {
        _gridSize = Mathf.Min(_gridSize + 1, _maxSize);
        RefreshGridCount();
    }

    private void RefreshGridCount()
    {
        _sizeLabel.Text = $"{_gridSize} by {_gridSize}";
    }

    public void OnMinusMatchPressed()
    {
        _matchCount = Mathf.Max(_matchCount - 1, _minSize);
        RefreshMatchCount();
    }

    public void OnPlusMatchPressed()
    {
        _matchCount = Mathf.Min(_matchCount + 1, _gridSize);
        RefreshMatchCount();
    }

    private void RefreshMatchCount()
    {
        _matchLabel.Text = $"{_matchCount} to win";
    }

    public void OnStartPressed()
    {
        var settings = (Settings)GetNode("/root/Settings");
        settings.GridSize = _gridSize;
        settings.MatchCount = _matchCount;

        var sceneChanger = (SceneChanger)GetNode("/root/SceneChanger");
        sceneChanger.GotoScene("res://Scenes/Main.tscn");
    }

    public void OnExitPressed() => GetTree().Quit();
}
