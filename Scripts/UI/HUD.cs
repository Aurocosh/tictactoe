using Godot;
using System;

public class HUD : Control
{
    private GameState _state;

    private Button _menuButton;
    private CenterContainer _gameMenu;

    public override void _Ready()
    {
        _state = GameState.InGameMenu;

        _menuButton = GetNode<Button>("MenuButton");
        _gameMenu = GetNode<CenterContainer>("GameMenu");

        Refresh();
    }

    private void OnGameStateChanged(ParWrapper gameStateWrapper)
    {
        _state = gameStateWrapper.GetValue<GameState>();
        Refresh();
    }

    private void Refresh()
    {
        _menuButton.Visible = _state == GameState.InGame;
        _gameMenu.Visible = _state == GameState.InGameMenu;
    }

    public void OnMenuClicked() => EmitSignal("MenuClicked");
    public void OnResumeClicked() => EmitSignal("ResumeClicked");
    public void OnRestartClicked() => EmitSignal("RestartClicked");
    public void OnExitToMenuClicked() => EmitSignal("ExitToMenuClicked");
    public void OnExitToWindowsClicked() => EmitSignal("ExitToWindowsClicked");

    [Signal]
    public delegate void MenuClicked();
    [Signal]
    public delegate void ResumeClicked();
    [Signal]
    public delegate void RestartClicked();
    [Signal]
    public delegate void ExitToMenuClicked();
    [Signal]
    public delegate void ExitToWindowsClicked();
}
