using Godot;
using System;

public class EndGameUI : Control
{
    private CellState _cellState;
    private GameState _gameState;

    private Label _messageLabel;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _cellState = CellState.Empty;
        _gameState = GameState.InGameMenu;
        _messageLabel = GetNode<Label>("Message/PanelContainer/VBoxContainer/MessageLabel");
        Refresh();
    }

    private void OnGameStateChanged(ParWrapper gameStateWrapper)
    {
        _gameState = gameStateWrapper.GetValue<GameState>();
        Refresh();
    }

    private void Refresh()
    {
        if (_gameState == GameState.WonGame)
        {
            var winningSide = _cellState == CellState.Cross ? "Cross" : "Nought";
            _messageLabel.Text = $"{winningSide} won!";
            Show();
        }
        else if (_gameState == GameState.Draw)
        {
            _messageLabel.Text = "It's a draw!";
            Show();
        }
        else
        {
            Hide();
        }
    }

    public void OnVictory(ParWrapper state, ParWrapper lineStart, ParWrapper lineEnd)
    {
        _cellState = state.GetValue<CellState>();
        Refresh();
    }

    public void OnRestartPressed() => EmitSignal("RestartPressed");
    public void OnExitPressed() => EmitSignal("ExitPressed");

    [Signal]
    public delegate void RestartPressed();
    [Signal]
    public delegate void ExitPressed();
}
