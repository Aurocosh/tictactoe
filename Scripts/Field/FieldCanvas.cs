using Godot;
using System;
using System.Collections.Generic;

public class FieldCanvas : Node2D
{
    private List<LineData> _linesToDraw;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _linesToDraw = new List<LineData>();
    }

    public void AddLine(Vector2 from, Vector2 to, Color color, float width = 1, bool antialiased = false)
    {
        var lineData = new LineData(from, to, color, width, antialiased);
        _linesToDraw.Add(lineData);
    }

    public void AddLine(LineData lineData)
    {
        _linesToDraw.Add(lineData);
    }

    public void ClearLines() => _linesToDraw.Clear();

    public override void _Draw()
    {
        foreach (var lineData in _linesToDraw)
        {
            DrawLine(lineData.From, lineData.To, lineData.Color, lineData.Width, lineData.Antialiased);
            DrawCircle(lineData.From, lineData.Width / 2, lineData.Color);
            DrawCircle(lineData.To, lineData.Width / 2, lineData.Color);
        }
    }
}
