using Godot;
using System;

public class Cell : TextureButton
{
    [Export]
    Texture Cross;
    [Export]
    Texture Nought;

    private TextureRect _textureRect;

    public Vector2Int Coordinate { get; set; }
    private CellState _state;
    public CellState State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
            RefreshImage();
        }
    }

    public override void _Ready()
    {
        _textureRect = GetNode<TextureRect>("TextureRect");
        if (_textureRect == null)
            GD.Print("Null");
    }

    public void Resize(int oldGridSize, int newGridSize)
    {
        RectMinSize = (RectMinSize * oldGridSize) / newGridSize;
        float margin = _textureRect.MarginLeft * oldGridSize / newGridSize;
        _textureRect.MarginLeft = margin;
        _textureRect.MarginTop = margin;
        _textureRect.MarginRight = -margin;
        _textureRect.MarginBottom = -margin;
    }

    private void RefreshImage()
    {
        switch (_state)
        {
            case CellState.Cross:
                _textureRect.Texture = Cross;
                break;
            case CellState.Nought:
                _textureRect.Texture = Nought;
                break;
            default:
                _textureRect.Texture = null;
                break;
        }
    }

    public void OnButtonClicked()
    {
        EmitSignal("CellClicked", new ParWrapper(Coordinate));
    }

    [Signal]
    public delegate void CellClicked(ParWrapper coordinate);
}
