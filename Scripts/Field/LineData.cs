using Godot;
using System;

public readonly struct LineData
{
    public Vector2 From { get; }
    public Vector2 To { get; }
    public Color Color { get; }
    public float Width { get; }
    public bool Antialiased { get; }

    public LineData(Vector2 from, Vector2 to, Color color, float width = 1, bool antialiased = false)
    {
        From = from;
        To = to;
        Color = color;
        Width = width;
        Antialiased = antialiased;
    }
}
