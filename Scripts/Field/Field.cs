using Godot;
using System;

public class Field : PanelContainer
{
    [Export]
    public PackedScene CellTemplate;
    private Grid<Cell> _grid;
    private GridContainer _field;
    private FieldCanvas _canvas;

    private CellState nextState;

    public int GridSize { get; private set; }

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GridSize = 3;
        _field = GetNode<GridContainer>("GridContainer");
        _canvas = GetNode<FieldCanvas>("Canvas");
    }

    public void InitializeGrid(ParWrapper wrapper)
    {
        ClearGrid();

        var grid = wrapper.GetValue<Grid<CellState>>();
        int oldGridSize = GridSize;
        GridSize = grid.Width;
        _field.Columns = GridSize;
        _grid = new Grid<Cell>(grid.Width, grid.Height);

        for (int x = 0; x < GridSize; x++)
        {
            for (int y = 0; y < GridSize; y++)
            {
                var cell = (Cell)CellTemplate.Instance();
                _field.AddChild(cell);
                cell.Resize(oldGridSize, GridSize);
                cell.Coordinate = new Vector2Int(x, y);
                cell.State = grid[x, y];
                cell.RectSize = cell.RectMinSize;
                cell.Connect("CellClicked", this, "OnCellClicked");
                _grid[x, y] = cell;
            }
        }

        _canvas.ClearLines();
        _canvas.Update();
    }

    private void ClearGrid()
    {
        foreach (Node node in _field.GetChildren())
            node.QueueFree();
        GridSize = 3;
    }

    public void SetCellState(ParWrapper coordinateWrapper, ParWrapper stateWrapper)
    {
        var coordinate = coordinateWrapper.GetValue<Vector2Int>();
        var state = stateWrapper.GetValue<CellState>();

        Cell cell = _grid[coordinate.X, coordinate.Y];
        cell.State = state;
    }

    public void OnVictory(ParWrapper state, ParWrapper lineStart, ParWrapper lineEnd)
    {
        var fromCoordinate = lineStart.GetValue<Vector2Int>();
        var toCoordinate = lineEnd.GetValue<Vector2Int>();

        var fromCell = _grid[fromCoordinate];
        var toCell = _grid[toCoordinate];

        _canvas.ClearLines();
        var transform = GetTransform();

        var fromRect = fromCell.GetGlobalRect();
        var fromCenter = fromRect.Position + fromRect.Size / 2;
        var fromCenterLocal = transform.XformInv(fromCenter);

        var toRect = toCell.GetGlobalRect();
        var toCenter = toRect.Position + toRect.Size / 2;
        var toCenterLocal = transform.XformInv(toCenter);

        var start = new Vector2(0, 0);
        _canvas.AddLine(fromCenterLocal, toCenterLocal, Colors.Red, 6, true);
        _canvas.Update();
    }

    public void OnCellClicked(ParWrapper coordinate) => EmitSignal("CellClicked", coordinate);

    [Signal]
    public delegate void CellClicked(ParWrapper coordinate);
}
