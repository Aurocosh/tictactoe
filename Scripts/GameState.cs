using Godot;
using System;

public enum GameState
{
    InGameMenu,
    InGame,
    WonGame,
    Draw
}
