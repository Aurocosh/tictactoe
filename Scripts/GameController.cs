using Godot;
using System;
using System.Collections.Generic;

public class GameController : Node
{
    private int _gridSize;
    private int _matchCount;
    private int _emptyCellCount;
    private CellState _nextState;
    private Grid<CellState> _grid;
    private GameState _gameState;

    public GameState State
    {
        get => _gameState;
        set
        {
            if (_gameState == value)
                return;
            _gameState = value;
            EmitSignal("GameStateChanged", new ParWrapper(value));
        }
    }

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        State = GameState.InGame;

        var settings = (Settings)GetNode("/root/Settings");
        _gridSize = settings.GridSize;
        _matchCount = settings.MatchCount;

        StartGame();
    }

    public void StartGame()
    {
        _nextState = CellState.Cross;
        State = GameState.InGame;
        _emptyCellCount = _gridSize * _gridSize;
        _grid = new Grid<CellState>(_gridSize, _gridSize);
        EmitSignal("GridInitialized", new ParWrapper(_grid));
    }

    public void OnCellClicked(ParWrapper wrapper)
    {
        if (_gameState != GameState.InGame)
            return;

        var coordinate = wrapper.GetValue<Vector2Int>();
        if (_grid[coordinate] != CellState.Empty)
            return;

        _emptyCellCount--;
        _grid[coordinate] = _nextState;
        EmitSignal("CellStateChanged", wrapper, new ParWrapper(_nextState));

        CheckWinCondition(coordinate, _nextState);
        _nextState = _nextState == CellState.Cross ? CellState.Nought : CellState.Cross;
    }

    private void CheckWinCondition(Vector2Int coordinate, CellState currentState)
    {
        List<Vector2Int> directions = new List<Vector2Int>() { Vector2IntConst.NegX, Vector2IntConst.PosY, Vector2IntConst.NegXPosY, Vector2IntConst.PosXPosY };
        foreach (var direction in directions)
        {
            int matches = 1;
            matches += CountMatches(currentState, direction, coordinate);
            matches += CountMatches(currentState, -direction, coordinate);

            if (matches >= _matchCount)
            {
                var lineStart = GetLastCoordinate(currentState, direction, coordinate);
                var lineEnd = GetLastCoordinate(currentState, -direction, coordinate);
                State = GameState.WonGame;
                EmitSignal("GameIsWon", new ParWrapper(currentState), new ParWrapper(lineStart), new ParWrapper(lineEnd));
            }
            else if (_emptyCellCount == 0)
            {
                State = GameState.Draw;
            }
        }
    }

    private Vector2Int GetLastCoordinate(CellState state, Vector2Int direction, Vector2Int coordinate)
    {
        var nextCoordinate = coordinate;
        while (_grid.IsItInBounds(nextCoordinate) && _grid[nextCoordinate] == state)
        {
            coordinate = nextCoordinate;
            nextCoordinate += direction;
        }
        return coordinate;
    }

    private int CountMatches(CellState currentState, Vector2Int direction, Vector2Int coordinate)
    {
        int matches = 0;
        var nextCoordinate = direction + coordinate;
        while (_grid.IsItInBounds(nextCoordinate))
        {
            var state = _grid[nextCoordinate.X, nextCoordinate.Y];
            if (state == currentState)
            {
                matches++;
                nextCoordinate += direction;
            }
            else
            {
                return matches;
            }
        }

        return matches;
    }

    public void OnMenuClicked()
    {
        State = GameState.InGameMenu;
    }

    public void OnResumeClicked()
    {
        State = GameState.InGame;
    }

    public void OnRestartClicked()
    {
        StartGame();
    }

    public void OnExitToMenuClicked()
    {
        var sceneChanger = (SceneChanger)GetNode("/root/SceneChanger");
        sceneChanger.GotoScene("res://Scenes/MainMenu.tscn");
    }

    public void OnExitToWindowsClicked()
    {
        GetTree().Quit();
    }

    [Signal]
    public delegate void CellStateChanged(ParWrapper coordinate, ParWrapper state);

    [Signal]
    public delegate void GameStateChanged(ParWrapper GameState);

    [Signal]
    public delegate void GridInitialized(ParWrapper grid);

    [Signal]
    public delegate void GameIsWon(ParWrapper state, ParWrapper lineStart, ParWrapper lineEnd);
}
